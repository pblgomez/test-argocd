#!/usr/bin/env bash

name='test-argocd'

kind create cluster --name "$name"
kubectl cluster-info --context kind-"$name"
kubectl --context kind-"$name" create namespace argocd
kubectl --context kind-"$name" apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

kubectl --context kind-"$name" get secret/argocd-initial-admin-secret -n argocd -o yaml | awk '/password/ {print $2}' | base64 -d | cut -f 1 | xclip
while [ $? -ne 0 ]; do
  kubectl --context kind-"$name" get secret/argocd-initial-admin-secret -n argocd -o yaml | awk '/password/ {print $2}' | base64 -d | cut -f 1 | tee xclip
done
kubectl --context kind-"$name" apply -f apps/.

sleep 5
kubectl --context kind-"$name" -n argocd port-forward svc/argocd-server 8443:443 &
xdg-open http://localhost:8443 &
